package com.company;

import java.util.*;

public class Video {
    private final Scanner cout = new Scanner(System.in);
    private String title;
    private Vector <Comment> comments = new Vector <Comment>();
    private Description description;
    Video next;
    Video(String title, Description description){
        this.title = title;
        this.description = description;
        next = null;
    }
    public void watch(){
        String answer;
        System.out.print("\nWatching " + this.title + "\n\nDescription:\n");
        description.print();
        System.out.println("Comments:");
        for(int i = 0; i < comments.size(); i++){
            comments.get(i).print();
        }
        do {
            System.out.print("Would you like to like a comment? ");
            answer = cout.nextLine();
            if(answer.contains("y")) {
                for(int i = 0; i < comments.size(); i++){
                    System.out.print(i + " ");
                    comments.get(i).print();
                }
                System.out.print("Which one? ");
                int comment_to_like = cout.nextInt();
                cout.nextLine();
                comments.get(comment_to_like).like();
            }else if(answer.contains("n")){
                // do nothing
            }else{
                System.out.println("Invalid answer, assuming no likes");
                answer = "n";
            }
        }while(!(answer.contains("n")));
        System.out.print("Would you like to leave a comment? ");
        answer = cout.nextLine();
        if(answer.contains("y")){
            comment();
        }else if(answer.contains("n")){
            return;
        }else{
            System.out.println("Not a valid answer, assuming no comment.");
        }
    }
    public void comment(){
        System.out.print("What is your username? ");
        String username = cout.nextLine();
        System.out.print("What is your comment? ");
        String comment = cout.nextLine();
        comments.add(new Comment(comment, username));
    }
    public void comment(String comment, String username, int likes){
        comments.add(new Comment(comment, username, likes));
    }
    public void printTitle(){
        System.out.println(this.title);
    }
}