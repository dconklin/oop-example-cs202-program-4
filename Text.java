package com.company;

abstract class Text {
    protected String text;
    Text (String text){
        this.text = text;
    }
    Text (){
        this.text = "Default text";
    }
    abstract void print();
}
