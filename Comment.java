package com.company;

public class Comment extends Text {
    private int likes;
    private final String username;

    Comment(String text, String username) {
        super(text);
        this.username = username;
    }
    Comment(String text, String username, int likes) {
        super(text);
        this.username = username;
        this.likes = likes;
    }
    Comment(){
        super("Default Comment");
        this.username = "Anonymous";
    }
    public void like(){
        this.likes++;
    }

    @Override
    void print() {
        System.out.println();
        System.out.println("User: " + this.username);
        System.out.println("\t" + this.text);
        System.out.println("\t" + this.likes + " Likes");
        System.out.println();
    }
}
