package com.company;

import java.util.*;

public class Main {
    private static final String prompt = ">>> ";
    private static final Scanner cout = new Scanner(System.in);
    private static final Vector<Playlist> list = new Vector<>();
    private static String input;

    public static void main(String[] args) {
        /*
         * Adding some default Playlists, Videos, and Comments
         */
        list.add(new Playlist("Introduction")   );
        list.add(new Playlist("Week 1"      )   );
        list.add(new Playlist("Week 2"      )   );
        list.add(new Playlist("Week 3"      )   );
        list.get(0).add_end(new Video("Welcome to My Class!",           new Description("Welcoming you to my class")        ));
        list.get(0).add_end(new Video("Getting Started",                new Description("Showing you how this class works") ));
        list.get(0).add_end(new Video("Going Over the Class Syllabus",  new Description("Reading the class syllabus")       ));
        list.get(1).add_end(new Video("Slideshow 1 for Week 1",         new Description("Reading week 1 slideshow")         ));
        list.get(1).add_end(new Video("Homework for Week 1",            new Description("Assigning week 1 homework")        ));
        list.get(2).add_end(new Video("Slideshow 2 for Week 2",         new Description("Reading week 2 slideshow")         ));
        list.get(2).add_end(new Video("Homework for Week 2",            new Description("Assigning week 2 homework")        ));
        list.get(3).add_end(new Video("Slideshow 3 for Week 3",         new Description("Reading week 3 slideshow")         ));
        list.get(3).add_end(new Video("Homework for Week 3",            new Description("Assigning week 3 homework")        ));
        list.get(0).get(0).comment("Thank you for making this!",                "Dylan Conklin",    90 );
        list.get(0).get(0).comment("Thank you for having us!",                  "Haley Johnson",    15 );
        list.get(0).get(0).comment("This class looks fun",                      "Devin Neville",    10 );
        list.get(0).get(1).comment("This helped me get started.",               "Natasha Estes",    40 );
        list.get(0).get(1).comment("Now I now how to get started!",             "Olin Munoz",       20 );
        list.get(0).get(1).comment("So I click on the Get Started tab, right?", "Delores William",  32 );
        list.get(0).get(2).comment("The syllabus looks nice",                   "Paul Hardy",       28 );
        list.get(0).get(2).comment("The syllabus is confusing",                 "Lavonne Simon",    82 );
        list.get(0).get(2).comment("What is Title IX?",                         "Aimee Marshall",   57 );
        list.get(1).get(0).comment("The slides are in beamer?",                 "Patty Kline",      73 );
        list.get(1).get(0).comment("LOL oUtDaTeD BeAmEr SlIdeS xD",             "Rowena Hodges",    58 );
        list.get(1).get(0).comment("It's okay, I like beamer.",                 "Lemuel Beach",     18 );
        list.get(1).get(1).comment("Topic 1 is pretty easy",                    "Krista Chambers",  74 );
        list.get(1).get(1).comment("Easier than expected",                      "Napoleon Everett", 13 );
        list.get(1).get(1).comment("How to do problem #1???",                   "Alec Cooley",      57 );
        list.get(2).get(0).comment("Oh boy, here's the hard stuff",             "Alicia Fernandez", 28 );
        list.get(2).get(0).comment("First beamer, and now Org Mode? Jeez",      "Daryl Molina",     73 );
        list.get(2).get(0).comment("You sound very quiet",                      "Jarrett Barr",     72 );
        list.get(2).get(1).comment("Thank you!",                                "Norma Ryan",       62 );
        list.get(2).get(1).comment("This is very helpful",                      "Daphne Huber",     14 );
        list.get(2).get(1).comment("This is super great",                       "Rena Foreman",     74 );
        list.get(3).get(0).comment("The slideshows are very well laid out",     "Lenore Howell",    28 );
        list.get(3).get(0).comment("Thank you for helping!",                    "Sheree Conley",    58 );
        list.get(3).get(1).comment("This homework is getting super hard",       "Gene Alvarez",     71 );
        list.get(3).get(1).comment("Thank goodness it's only 1-3, parts a - z", "Shelley Jensen",   47 );
        do{
            input = "null";
            System.out.print(prompt);
            input = cout.nextLine();
            if (input.contains("add")) {
                add();
            } else if (input.contains("list")) {
                listAll();
            } else if (input.contains("watch")) {
                view();
            }
        }while(!(input.contains("quit")));
    }
    public static void add() {
        if(input.contains("playlist")) {
            System.out.print("Would you like to add a title? ");
            input = cout.nextLine();
            if(input.contains("n")) {
                list.add(new Playlist());
            }else if(input.contains("y")) {
                System.out.print("Enter title: ");
                String title = cout.nextLine();
                list.add(new Playlist(title));
            }else{
                System.out.println("Invalid option, playlist not added.");
            }
        }else if(input.contains("video")){
            upload();
        }
    }
    public static void listPlaylists(){
        for(int i = 0; i < list.size(); i++){
            System.out.print(i + " ");
            list.get(i).printTitle();
        }
    }
    public static void upload(){
        listPlaylists();
        System.out.print("Which week would you like to upload to? ");
        int week = cout.nextInt();
        cout.nextLine();
        System.out.print("What is the title of the video? ");
        String title = cout.nextLine();
        System.out.print("Please enter a description: ");
        String description = cout.nextLine();
        list.get(week).add_end(new Video(title, new Description(description)));
    }
    public static void listAll(){
        for(int i = 0; i < list.size(); i++){
            System.out.println(i + ") ");
            list.get(i).printTitle();
            list.get(i).print();
            System.out.println();
        }
    }
    public static void view(){
        listPlaylists();
        System.out.print("Which week would you like to watch? ");
        int week = cout.nextInt();
        cout.nextLine();
        list.get(week).watch();
    }
}