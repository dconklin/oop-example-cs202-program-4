package com.company;

public class Description extends Text {
    Description(String text) {
        super(text);
    }
    Description() {
        super("Default Description");
    }
    @Override
    void print() {
        System.out.println(this.text);
    }
}
