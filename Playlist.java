package com.company;

import java.util.*;

public class Playlist<V> {
    private final Scanner cout = new Scanner(System.in);
    private String title;
    private Video head;
    Playlist(){
    }
    Playlist(String title){
        this.title = title;
    }
    public void add_beg(Video video){
        video.next = head;
        head = video;
    }
    // public void add_end(String title, Description description){
    public void add_end(Video video){
        if(head == null) head = video;
        else add_endR(video, head);
    }
    private void add_endR(Video video, Video curr) {
        if(curr.next == null) curr.next = video;
        else add_endR(video, curr.next);
    }
    public void print(){
        if(head != null) printR(head, 0);
        return;
    }
    private void printR(Video node, int num){
        System.out.print(num + " ");
        node.printTitle();
        if(node.next != null) printR(node.next, num+1);
        else return;
    }
    public void delete(int pos){
        if(head == null) return;
        Video temp = head;
        if(pos == 0){
            head = head.next;
            return;
        }
        for(int i = 0; temp != null && i < pos- 1; i++){
            temp = temp.next;
        }
        if(temp == null || temp.next == null){
            return;
        }
        Video next = temp.next.next;
        temp.next = next;
        return;
    }
    private void deleteR(int pos){
    }
    public void deleteAll(){
    }
    public int size(){
        if(head != null) return sizeR(head);
        return 0;
    }
    private int sizeR(Video node){
        if(node.next != null) return sizeR(node.next) + 1;
        else return 1;
    }
    public Video get(int n){
        if(n < 0 || n >= size()) return null;
        Video return_video = null;
        if(this.head != null){
            if(n == 0) return_video = this.head;
            else return_video = getR(this.head, 0, n);
        }
        return return_video;
    }
    private Video getR(Video curr, int pos, int n){
        if(pos+1 == n) return curr.next;
        else return getR(curr.next, ++pos, n);
    }
    public void watch(){
        print();
        System.out.print("Which video? ");
        int videoNum = cout.nextInt();
        cout.nextLine();
        get(videoNum).watch();
    }
    public void printTitle(){
        System.out.println(this.title);
    }
}